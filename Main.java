package javafx;



import common.PasswordHasher;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

    private BorderPane rootLayout;

    //must be inserted
    //user: test
    //pass: test
    //passHashed: Xvc2W3TVEwVxoJNqdJQHhLK92FH8Ho0zdj+SiC4UQB8=$YljchaAi+cZY939qfPmbjZl62mE8qxz5AnJ3+NVB9ZA=
    @Override
    public void start(Stage primaryStage) throws Exception {

        AppNavigation.setStage(primaryStage);
        AppNavigation.getStage().setTitle("Rss Feed Reader");

        //displayMenu();

        dispayLogIn();
        //showContent();
    }

    private void dispayLogIn() {

        Pane logInLayout = (Pane) FXMLResources.load(FXMLResources.login);

        Scene scene = new Scene(logInLayout);

        AppNavigation.getStage().setScene(scene);
        AppNavigation.getStage().show();
    }




    @FXML
    public static void main(String[] args) {
        launch(args);
    }
}
