package javafx.controller.comments;

import common.CurrentUser;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.controller.friend.Keeper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Comment;
import javafx.model.User;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import services.CommentService;

import java.net.URL;
import java.util.ResourceBundle;


public class DisplayCommentsController implements Initializable {

    @FXML
    private TableView<Comment> commentsTable;
    @FXML
    private TableColumn<Comment, String> titleColumn;
    @FXML
    private TableColumn<Comment, String> userColumn;

    private ObservableList<Comment> comments;

    private CommentService commentService;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        commentService = new CommentService();
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().contentProperty());
        userColumn.setCellValueFactory(cellData -> cellData.getValue().getFriend().userNameProperty());
        comments = FXCollections.observableArrayList();

        comments.clear();
        comments.addAll(commentService.getForNews(Keeper.getNews().getId()));

        commentsTable.setItems(comments);
    }
}
