package javafx.controller.comments;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Comment;
import javafx.model.Friendship;
import javafx.model.News;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import services.FriendshipService;

import java.net.URL;
import java.util.ResourceBundle;

public class AddCommentsController implements Initializable {

    @FXML
    public TextField contentField;
    @FXML
    private TableView<Comment> friendsTable;
    @FXML
    private TableColumn<Comment, String> friendsColumn;

    @FXML
    private TextField friendNameField;

    private FriendshipService friendshipService;
    private ObservableList<Friendship> friendships;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void addAction(ActionEvent actionEvent) {
    }
}
