package domain.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Name")
    private String name;


    @Column(name = "Description")
    private String description;

    @ManyToMany()
    @JoinTable(name="CategoryKeywords",
        joinColumns = @JoinColumn(name = "CategoryId", referencedColumnName = "Id"),
        inverseJoinColumns = @JoinColumn(name = "KeywordId", referencedColumnName = "Id")
    )
    private List<Keyword> keywords;


    public Category() {
    }

    public Category(Long id) {
        this.id = id;
    }

    public Category( String name, String description, List<Keyword> keywords) {
        this.name = name;
        this.description = description;
        this.keywords = keywords;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
