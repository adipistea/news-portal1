package javafx;

import javafx.fxml.FXMLLoader;

import java.io.IOException;


public class FXMLResources {
    public static final String login = "view/LogIn.fxml";

    public static final String rootLayout = "view/RootLayout.fxml";

    public static final String content = "view/Content.fxml";

    public static final String addNews = "view/news/AddNews.fxml";

    public static final String displayNews = "view/news/DisplayNews.fxml";

    public static final String addCategory ="view/category/AddCategory.fxml";

    public static final String displayCategories="view/category/DisplayCategory.fxml";

    public static final String displayAllFriends ="view/friend/DisplayAllFriends.fxml";

    public static final String requests = "view/friend/Requests.fxml";

    public static final String giveRights = "view/friend/GiveRights.fxml";

    public static final String displayComments = "view/comments/DisplayComments.fxml";

    public static final String friendNews = "view/friend/FriendNews.fxml";

    public static final String addCommentForFriend = "view/friend/AddCommentsForFriend.fxml";

    public static final String assignToCategory = "view/news/AssignToCategory.fxml";

    public static Object load(String resourceLocation){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource(resourceLocation));

            return loader.load();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
