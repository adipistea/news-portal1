package common.mapping;


import domain.entities.News;

import java.util.Date;
import java.util.List;


public class NewsToNewsDb implements GenericMapper<javafx.model.News, News> {

    private UserToUserDb userToUserDb;
    private SiteToSiteDb siteToSiteDb;
    private CategoryToCategoryDb categoryToCategoryDb;


    public NewsToNewsDb() {
        this.userToUserDb = new UserToUserDb();
        this.siteToSiteDb = new SiteToSiteDb();
        this.categoryToCategoryDb = new CategoryToCategoryDb();
    }

    @Override
    public News map(javafx.model.News source) {

        Date date = new Date();

        java.sql.Date sqlDate = new java.sql.Date(date.getTime());

        return source!=null ? new News(source.getId(),"", source.getTitle(), sqlDate,"",
                siteToSiteDb.map(source.getSite()),
                userToUserDb.map(source.getUser()),
                categoryToCategoryDb.map(source.getCategory())) : null;
    }
}
