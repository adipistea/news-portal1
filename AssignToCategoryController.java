package javafx.controller.news;

import com.sun.org.apache.bcel.internal.generic.NEW;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.controller.friend.Keeper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Category;
import javafx.scene.control.ComboBox;
import services.CategoryService;
import services.NewsService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


public class AssignToCategoryController implements Initializable {
    public ComboBox<String> categories;
    private CategoryService categoryService;
    private NewsService newsService;
    @FXML
    private ObservableList<String> categoriesList;

    private List<Category> categoryList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newsService = new NewsService();
        categoryService = new CategoryService();

        categoriesList = FXCollections.observableArrayList();


        categoryList = categoryService.get();
        for (Category category : categoryList) {
            categoriesList.add(category.getName());
        }
        categories.setItems(categoriesList);
    }


    public void save(ActionEvent actionEvent) {
        String selectedName = categories.getSelectionModel().getSelectedItem();

        if (selectedName != null) {
            for (Category category : categoryList) {
                if (category.getName().equals(selectedName)) {
                    newsService.updateCategory(Keeper.getNews(), category.getId());
                    break;
                }
            }
        }
    }
}
