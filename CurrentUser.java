package common;

import domain.entities.User;


public class CurrentUser {

    private static User loggedInUser;

    public static User getLoggedInUser() {
        return loggedInUser;
    }

    public static void setLoggedInUser(User loggedInUser) {
        CurrentUser.loggedInUser = loggedInUser;
    }
}
