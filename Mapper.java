package common.mapping;


public class Mapper {
    private NewsDbToNews newsDbToNews;
    private NewsToNewsDb newsToNewsDb;
    private CommentToCommentDb commentToCommentDb;
    private CommentDbToComment commentDbToComment;
    private FriendshipDbToFriendship friendshipDbToFriendship;
    private FriendshipToFriendshipDb friendshipToFriendshipDb;
    private RightDbToRight rightDbToRight;
    private RightToRightDb rightToRightDb;
    private SiteToSiteDb siteToSiteDb;
    private SiteDbToSite siteDbToSite;
    private UserDbToUser userDbToUser;
    private UserToUserDb userToUserDb;
    private CategoryToCategoryDb categoryToCategoryDb;
    private CategoryDbToCategory categoryDbToCategory;

    public Mapper() {
        this.newsDbToNews = new NewsDbToNews();
        this.newsToNewsDb = new NewsToNewsDb();
        this.commentToCommentDb = new CommentToCommentDb();
        this.commentDbToComment = new CommentDbToComment();
        this.friendshipDbToFriendship = new FriendshipDbToFriendship();
        this.friendshipToFriendshipDb = new FriendshipToFriendshipDb();
        this.rightDbToRight = new RightDbToRight();
        this.rightToRightDb = new RightToRightDb();
        this.siteToSiteDb = new SiteToSiteDb();
        this.siteDbToSite = new SiteDbToSite();
        this.userDbToUser = new UserDbToUser();
        this.userToUserDb = new UserToUserDb();
        this.categoryToCategoryDb = new CategoryToCategoryDb();
        this.categoryDbToCategory = new CategoryDbToCategory();
    }

    public CategoryToCategoryDb getCategoryToCategoryDb() {
        return categoryToCategoryDb;
    }

    public CategoryDbToCategory getCategoryDbToCategory() {
        return categoryDbToCategory;
    }

    public CommentDbToComment getCommentDbToComment() {
        return commentDbToComment;
    }

    public NewsDbToNews getNewsDbToNews() {
        return newsDbToNews;
    }

    public NewsToNewsDb getNewsToNewsDb() {
        return newsToNewsDb;
    }

    public CommentToCommentDb getCommentToCommentDb() {
        return commentToCommentDb;
    }

    public FriendshipDbToFriendship getFriendshipDbToFriendship() {
        return friendshipDbToFriendship;
    }

    public FriendshipToFriendshipDb getFriendshipToFriendshipDb() {
        return friendshipToFriendshipDb;
    }

    public RightDbToRight getRightDbToRight() {
        return rightDbToRight;
    }

    public RightToRightDb getRightToRightDb() {
        return rightToRightDb;
    }

    public SiteToSiteDb getSiteToSiteDb() {
        return siteToSiteDb;
    }

    public SiteDbToSite getSiteDbToSite() {
        return siteDbToSite;
    }

    public UserDbToUser getUserDbToUser() {
        return userDbToUser;
    }

    public UserToUserDb getUserToUserDb() {
        return userToUserDb;
    }
}
