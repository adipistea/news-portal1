package infrastructure.data.dao;

import infrastructure.data.interfaces.*;

public class UnitOfWork implements IUnitOfWork {

    private IUserDAO userDAO;
    private INewsDAO newsDAO;
    private ICategoryDAO categoryDAO;
    private ISiteDAO siteDAO;
    private IFriendshipDAO friendshipDAO;
    private IRightDAO rightDAO;
    private ICommentDAO commentDAO;


    public UnitOfWork() {
        this.userDAO = new UserDAO();
        this.newsDAO = new NewsDAO();
        this.categoryDAO = new CategoryDAO();
        this.siteDAO = new SiteDAO();
        this.friendshipDAO = new FriendshipDAO();
        this.rightDAO = new RightDAO();
        this.commentDAO = new CommentDAO();
    }

    public IUserDAO getUserDAO() {
        return userDAO;
    }

    public INewsDAO getNewsDAO() {
        return newsDAO;
    }

    public ICategoryDAO getCategoryDAO() {
        return categoryDAO;
    }

    public ISiteDAO getSiteDAO() {
        return siteDAO;
    }

    public IFriendshipDAO getFriendshipDAO() {
        return friendshipDAO;
    }

    public IRightDAO getRightDAO(){return  rightDAO;}

    public ICommentDAO getCommentDAO(){return commentDAO;}
}
