package services;

import domain.entities.Site;
import infrastructure.data.dao.UnitOfWork;
import javafx.model.CrawledNews;
import crawler.Crawler;
import services.interfaces.ICrawlService;

import java.util.ArrayList;
import java.util.List;


public class CrawlService implements ICrawlService {

    private Crawler crawler;
    private UnitOfWork unitOfWork;
    private Site rssSite;

    public CrawlService() {
        this.crawler = new Crawler();
        unitOfWork = new UnitOfWork();
    }

    @Override
    public List<CrawledNews> crawl(String url) {
        rssSite = unitOfWork.getSiteDAO().getRSSFeedSite(url);

        if(rssSite == null)
            return null;

        return crawlRssUrl(rssSite.getRssFeedUrl());
    }

    public List<CrawledNews> crawlRssUrl(String url){
        List<crawler.CrawledNews> news= crawler.startCrawling(url);
        List<CrawledNews> newsToReturn = mapCrawledNews(news);
        return newsToReturn;
    }


    private List<CrawledNews> mapCrawledNews(List<crawler.CrawledNews> news) {
        List<CrawledNews> newsToReturn = new ArrayList<>();
        for(crawler.CrawledNews value : news){
            newsToReturn.add(new CrawledNews(value.getTitle(), value.getLink(), value.getPubDate()));
        }
        return newsToReturn;
    }
}
