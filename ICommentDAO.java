package infrastructure.data.interfaces;

import domain.entities.Comment;

import java.util.List;


public interface ICommentDAO extends IGenericDAO<Comment> {

    List<Comment> getCommentsForUser(Long id);

    List<Comment> getCommentsForNews(Long newsId);
}
