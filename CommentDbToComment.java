package common.mapping;

import domain.entities.Comment;


public class CommentDbToComment implements GenericMapper<Comment, javafx.model.Comment> {

    private UserDbToUser userDbToUser;
    private NewsDbToNews newsDbToNews;

    public CommentDbToComment() {
        this.userDbToUser = new UserDbToUser();
        this.newsDbToNews = new NewsDbToNews();
    }

    @Override
    public javafx.model.Comment map(Comment comment) {



        return comment!=null ? new javafx.model.Comment(comment.getId(), comment.getContent(), comment.getDateAdded(),
                newsDbToNews.map(comment.getNews()),
                userDbToUser.map(comment.getFriend())) : null;
    }
}
