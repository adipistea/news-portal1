package common.mapping;

import java.util.ArrayList;
import java.util.List;


public interface GenericMapper<TSource, TDestination> {


    TDestination map(TSource source);

//    List<TDestination> mapList(List<TSource> listSource);

    default List<TDestination> mapList(List<TSource> listSource) {
//        return new GenericListMapper(this).mapList(listSource);
        List<TDestination> list = new ArrayList<>();
        for(TSource entity  : listSource){
            list.add(map(entity));
        }
        return  list;
    }
}