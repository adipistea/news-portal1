package javafx;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class AppNavigation {

    private static Stage stage;

    public static Scene getScene() {
        return scene;
    }

    public static void setScene(Scene scene) {
        AppNavigation.scene = scene;
    }

    private static Scene scene;

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        AppNavigation.stage = stage;
    }

    private static BorderPane rootLayout;

    public static BorderPane getRootLayout() {
        return rootLayout;
    }

    public static void setRootLayout(BorderPane rootLayout) {
        AppNavigation.rootLayout = rootLayout;
    }
}
