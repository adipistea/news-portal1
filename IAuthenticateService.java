package services.interfaces;


public interface IAuthenticateService {

    boolean authenticate(String userName, String password);

    void preserveLoggedUser(String userName);
}
