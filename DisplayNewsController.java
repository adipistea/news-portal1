package javafx.controller.news;

import common.Browser;
import common.CurrentUser;
import javafx.AppNavigation;
import javafx.FXMLResources;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.controller.friend.Keeper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Category;
import javafx.model.News;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import services.NewsService;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;


public class DisplayNewsController implements Initializable {

    public TableColumn<News, String> titleColumn;
    public TableColumn<News, String> linkColumn;
    public TableColumn<News, Date> dateColumn;
    public TableColumn<News, String> categoryColumn;
    @FXML
    private TableView<News> newsTable;

    private ObservableList<News> newsList;
    private NewsService newsService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newsService = new NewsService();
        newsList = FXCollections.observableArrayList();

        // Initialize the person table with the two columns.
        categoryColumn.setCellValueFactory(cellData -> cellData.getValue().getCategory() != null
                ? cellData.getValue().getCategory().nameProperty() : new SimpleStringProperty(""));
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        linkColumn.setCellValueFactory(cellData -> cellData.getValue().linkProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().pubDateProperty());
        newsList.clear();
        newsList.addAll(newsService.get(CurrentUser.getLoggedInUser().getId()));

        newsTable.setItems(newsList);
    }

    public void deleteAction(ActionEvent event) {
        News selectedItem = newsTable.getSelectionModel().getSelectedItem();

        newsService.delete(selectedItem.getId());

        newsList.remove(selectedItem);
    }

    public void viewCommentsAction(ActionEvent event) {
        News selectedItem = newsTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Keeper.setNews(selectedItem);

            AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.displayComments));
        }
    }

    public void navigateAction(ActionEvent actionEvent) {
        News selectedItem = newsTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Browser browser = new Browser();
            browser.start(new Stage());
            browser.navigate(selectedItem.getLink());
        }
    }

    public void assignToCategoryAction(ActionEvent actionEvent) {
        News selectedItem = newsTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Keeper.setNews(selectedItem);

            AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.assignToCategory));
        }
    }
}
