package services;

import common.mapping.CommentToCommentDb;
import common.mapping.Mapper;
import common.mapping.NewsDbToNews;
import common.mapping.NewsToNewsDb;
import infrastructure.data.dao.UnitOfWork;
import infrastructure.data.interfaces.IUnitOfWork;
import javafx.model.Comment;

import java.util.ArrayList;
import java.util.List;


public class CommentService {

    private IUnitOfWork unitOfWork;
    private Mapper mapper;

    public CommentService() {
        unitOfWork = new UnitOfWork();
        mapper = new Mapper();
    }


    public List<Comment> getForUser(Long id){

        List<domain.entities.Comment> commentsDb = unitOfWork.getCommentDAO().getCommentsForUser(id);

        return mapper.getCommentDbToComment().mapList(commentsDb);
    }

    public List<Comment> getForNews(Long newsId){

        List<domain.entities.Comment> commentsDb = unitOfWork.getCommentDAO().getCommentsForNews(newsId);

        return mapper.getCommentDbToComment().mapList(commentsDb);
    }

    public void add(Comment comment){

        domain.entities.Comment commentEntity = mapper.getCommentToCommentDb().map(comment);

        unitOfWork.getCommentDAO().create(commentEntity);
    }

}
