package domain.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Keywords")
public class Keyword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private Long id;

    @Column(name="Name")
    private String name;

    @ManyToMany()
    private List<Category> Categories;


    public Keyword(){}

    public Keyword(Long id){this.id = id;}

    public Keyword(Long id, String name, List<Category> categories) {
        this.id = id;
        this.name = name;
        Categories = categories;
    }
}
