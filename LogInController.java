package javafx.controller;

import javafx.AppNavigation;
import javafx.FXMLResources;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import services.AuthenticateService;
import services.interfaces.IAuthenticateService;

import java.net.URL;
import java.util.ResourceBundle;


public class LogInController implements Initializable {
    private IAuthenticateService authenticateService;

    @FXML
    private TextField userNameField;

    @FXML
    private PasswordField passwordField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        authenticateService = new AuthenticateService();
    }

    public void loginAction(ActionEvent event){

        if(authenticateService.authenticate(userNameField.getText(), passwordField.getText()))
        {
            authenticateService.preserveLoggedUser(userNameField.getText());

            displayMenu();
        }
    }



    public void displayMenu() {

        BorderPane rootLayout = (BorderPane) FXMLResources.load(FXMLResources.rootLayout);
        AppNavigation.setRootLayout(rootLayout);

        Scene scene = new Scene(rootLayout);

        AppNavigation.getStage().setScene(scene);
        AppNavigation.getStage().show();

        //show Content
//        AnchorPane content = (AnchorPane) FXMLResources.load(FXMLResources.content);
//        rootLayout.setCenter(content);
    }
}
