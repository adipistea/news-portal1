package domain.entities;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;


@Entity
@Table(name = "News3")
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Content")
    private String content;

    @Column(name = "Title")
    private String title;

    @Column(name = "DataAdded")
    private Date dataAdded;

    @Column(name = "Link")
    private String link;

    @JoinColumn(name = "SiteId", referencedColumnName = "Id")
    @ManyToOne
    private Site site;

    @JoinColumn(name = "UserId", referencedColumnName = "Id")
    @ManyToOne
    private User user;

    @JoinColumn(name = "CategoryId", referencedColumnName = "Id")
    @ManyToOne
    private Category category;

    public News() {
    }

    public News(String title) {
        this.title = title;
    }

    public News(String content, String title, Date dataAdded, String link, Site site, User user) {

        this.link = link;
        this.content = content;
        this.title = title;
        this.dataAdded = dataAdded;
        this.site = site;
        this.user = user;
    }

    //    public News(Long id,  String content, String title, Date dataAdded, String link, Site site, User user) {
//        this.id = id;
//        this.content = content;
//        this.title = title;
//        this.dataAdded = dataAdded;
//        this.link = link;
//        this.site = site;
//        this.user = user;
//    }
    public News(String content, String title, Date dataAdded, String link, Site site, User user, Category category) {

        this.content = content;
        this.title = title;
        this.dataAdded = dataAdded;
        this.link = link;
        this.site = site;
        this.user = user;
        this.category = category;
    }


    public News(Long id, String content, String title, Date dataAdded, String link, Site site, User user, Category category) {
        this.id = id;
        this.content = content;
        this.title = title;
        this.dataAdded = dataAdded;
        this.link = link;
        this.site = site;
        this.user = user;
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public Date getDataAdded() {
        return dataAdded;
    }

    public String getLink() {
        return link;
    }

    public Site getSite() {
        return site;
    }

    public User getUser() {
        return user;
    }


}
