package javafx.controller.category;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.model.Category;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import services.CategoryService;

import java.net.URL;
import java.util.ResourceBundle;


public class DiplayCategoryController implements Initializable{
    @FXML
    private TableView<Category> categoryTable;
    @FXML
    private TableColumn<Category, String> nameColumn;

    private ObservableList<Category> categoryList;
    private CategoryService categoryService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        categoryService = new CategoryService();
        // Initialize the person table with the two columns.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        categoryList = FXCollections.observableArrayList();

        categoryList.clear();
        categoryList.addAll(categoryService.get());

        categoryTable.setItems(categoryList);
    }

    public void deleteAction(ActionEvent event){
        Category c=categoryTable.getSelectionModel().getSelectedItem();
        categoryService.delete(c.getId());
        categoryList.remove(c);
    }
}
