package javafx.controller;

import javafx.AppNavigation;
import javafx.FXMLResources;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;


public class RootLayoutController implements Initializable{

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void addCategoryAction(ActionEvent event){

        AppNavigation.getRootLayout().setCenter((Pane)FXMLResources.load(FXMLResources.addCategory));
    }


    public void displayAllCategoriesAction(ActionEvent event){
        AppNavigation.getRootLayout().setCenter((Pane)FXMLResources.load(FXMLResources.displayCategories));
    }

    public void addNewsAction(ActionEvent event){
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.addNews));
    }

    public void displayAllNewsAction(ActionEvent event){
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.displayNews));
    }

    public void logOutAction(ActionEvent event){
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.login));
    }

    public void displayAllFriendsAction(ActionEvent event){
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.displayAllFriends));
    }

    public void friendRequestsAction(ActionEvent event){
        AppNavigation.getRootLayout().setCenter((Pane) FXMLResources.load(FXMLResources.requests));
    }
}
