package infrastructure.data.interfaces;

import domain.entities.Friendship;

import java.util.List;


public interface IFriendshipDAO extends IGenericDAO<Friendship> {
    List<Friendship> getFriendhipsForUser(Long id);

    List<Friendship> getFriendhipsForUser2(Long id);

}
