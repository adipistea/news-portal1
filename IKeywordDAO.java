package infrastructure.data.interfaces;

import domain.entities.Keyword;


public interface IKeywordDAO extends IGenericDAO<Keyword> {
}
