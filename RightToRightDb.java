package common.mapping;

import domain.entities.Right;

import java.util.List;


public class RightToRightDb implements GenericMapper<javafx.model.Right, Right> {
    @Override
    public Right map(javafx.model.Right source) {

        return source!=null ? new Right(source.getName()) : null;
    }

    @Override
    public List<Right> mapList(List<javafx.model.Right> listSource) {
        return new GenericListMapper<javafx.model.Right, Right>(this).mapList(listSource);
    }
}
