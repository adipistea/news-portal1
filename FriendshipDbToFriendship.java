package common.mapping;

import domain.entities.Friendship;
import javafx.model.Right;


public class FriendshipDbToFriendship implements GenericMapper<Friendship, javafx.model.Friendship> {

    private UserDbToUser userDbToUser;
    private RightDbToRight rightDbToRight;

    public FriendshipDbToFriendship() {
        this.userDbToUser = new UserDbToUser();
        this.rightDbToRight = new RightDbToRight();
    }

    @Override
    public javafx.model.Friendship map(Friendship friendship) {

        return friendship!=null ?  new javafx.model.Friendship(friendship.getId(),
                userDbToUser.map(friendship.getUser()),
                userDbToUser.map(friendship.getFriend()),
                (Right) rightDbToRight.mapList(friendship.getRights())) : null;
    }
}
