package common.mapping;


import domain.entities.FriendRequest;
import javafx.model.Friendship;


public class FriendshipToFriendshipDb implements GenericMapper<Friendship, domain.entities.Friendship> {
    private UserToUserDb userToUserDb;
    private RightToRightDb rightToRightDb;

    public FriendshipToFriendshipDb() {
        this.userToUserDb = new UserToUserDb();
        this.rightToRightDb = new RightToRightDb();
    }

    @Override
    public domain.entities.Friendship map(Friendship friendship) {

        return friendship!=null ? new domain.entities.Friendship(userToUserDb.map(friendship.getUser()),
                userToUserDb.map(friendship.getFriend()),
                FriendRequest.Unanswered,
                null
                ) : null;
    }
}
