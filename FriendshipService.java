package services;

import common.CurrentUser;
import common.DatabaseException;
import common.mapping.Mapper;
import domain.entities.FriendRequest;
import javafx.AppNavigation;
import javafx.model.Right;
import infrastructure.data.dao.UnitOfWork;
import infrastructure.data.interfaces.IUnitOfWork;
import javafx.model.Friendship;
import javafx.model.User;
//import jfx.messagebox.MessageBox;

import java.util.ArrayList;
import java.util.List;


public class FriendshipService {

    private IUnitOfWork unitOfWork;
    private Mapper mapper;

    public FriendshipService() {
        this.unitOfWork = new UnitOfWork();
        this.mapper = new Mapper();
    }

    public List<Friendship> get() {

        List<domain.entities.Friendship> friendshipsDb = null;

        friendshipsDb = unitOfWork.getFriendshipDAO().getFriendhipsForUser2(CurrentUser.getLoggedInUser().getId());

        removeDuplicateDataAndReorder(friendshipsDb);

        List<Friendship> friendships = map(friendshipsDb);

        return friendships;
    }

    private void removeDuplicateDataAndReorder(List<domain.entities.Friendship> friendshipsDb) {
        Long currentUserId = CurrentUser.getLoggedInUser().getId();

        for (int i = 0; i < friendshipsDb.size(); ) {

            domain.entities.Friendship friendship = friendshipsDb.get(i);
            if (friendship.getFriend().getId().equals(currentUserId)) {
                friendshipsDb.remove(friendship);
            }
            else{i++;}
        }
    }

    public boolean sendFriendRequest(domain.entities.User user, String friendName) {

        domain.entities.User friendDb = unitOfWork.getUserDAO().findByUserName(friendName);

        if (friendDb == null)
            return false;
        domain.entities.Friendship friendship = new domain.entities.Friendship(user, friendDb, FriendRequest.Unanswered, null);

        unitOfWork.getFriendshipDAO().create(friendship);

        return true;
    }

    public void delete(Long id) {
        unitOfWork.getFriendshipDAO().remove(id);
    }

    public void create(domain.entities.User user, User friend, List<Right> rights) {

        domain.entities.User friendDb = unitOfWork.getUserDAO().findByUserName(friend.getUserName());
        List<domain.entities.Right> rightList = new ArrayList<>();

        for (Right right : rights) {
            domain.entities.Right rightEntity = new domain.entities.Right(right.getName());
            rightList.add(rightEntity);
        }

        domain.entities.Friendship friendship = new domain.entities.Friendship(user, friendDb, FriendRequest.Unanswered, rightList);
        unitOfWork.getFriendshipDAO().create(friendship);

    }


    public List<Friendship> getAllFriendRequests(domain.entities.User user) {

        List<domain.entities.Friendship> friendships = unitOfWork.getFriendshipDAO().getFriendhipsForUser(user.getId());

        return map(friendships);
    }

    private List<Friendship> map(List<domain.entities.Friendship> friendshipsDb) {
        List<Friendship> friendships = new ArrayList<>();

        for (domain.entities.Friendship f : friendshipsDb) {
            domain.entities.User userDb = f.getUser();
            User user = new User(userDb.getId(), userDb.getFirstName(), userDb.getLastName(), userDb.getUsername());
            domain.entities.User friendDb = f.getFriend();
            User frind = new User(friendDb.getId(), friendDb.getFirstName(), friendDb.getLastName(), friendDb.getUsername());
            friendships.add(new Friendship(f.getId(), user, frind, null));
        }

        return friendships;
    }

    public void acceptFriendRequest(Friendship friendship) {
        domain.entities.Friendship friendshipDb = unitOfWork.getFriendshipDAO().find(friendship.getId());

        friendshipDb.setAcceptedFriend(FriendRequest.Accepted);

        unitOfWork.getFriendshipDAO().update(friendshipDb);

        domain.entities.Friendship friendshipR = new domain.entities.Friendship(friendshipDb.getFriend(), friendshipDb.getUser(), FriendRequest.Accepted, null);
        unitOfWork.getFriendshipDAO().create(friendshipR);
    }

    public void declineFriendRequest(Friendship friendship) {
        domain.entities.Friendship friendshipDb = unitOfWork.getFriendshipDAO().find(friendship.getId());

        friendshipDb.setAcceptedFriend(FriendRequest.Declined);

        unitOfWork.getFriendshipDAO().update(friendshipDb);
    }
}
