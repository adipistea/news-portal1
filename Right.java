package domain.entities;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "Rights")
public class Right {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private Long id;

    @Column(name="Name")
    private String name;

    @ManyToMany()
    private List<Friendship> friendships;

    public Right(){}

    public Right(Long id){this.id = id;}

    public Right(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
