package infrastructure.data.interfaces;

import domain.entities.Site;

public interface ISiteDAO extends IGenericDAO<Site> {

    Site getRSSFeedSite(String url);
}
