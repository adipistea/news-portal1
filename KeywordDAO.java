package infrastructure.data.dao;

import domain.entities.Keyword;
import infrastructure.data.interfaces.IKeywordDAO;


public class KeywordDAO extends GenericDAO<Keyword> implements IKeywordDAO {
    public KeywordDAO() {
        super(Keyword.class);
    }
}
