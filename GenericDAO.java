package infrastructure.data.dao;

import common.DatabaseException;
import infrastructure.JpaUtil;
import infrastructure.data.interfaces.IGenericDAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;


public class GenericDAO<T> implements IGenericDAO<T> {

    private Class<T> type;

    public GenericDAO(Class<T> type) {
        this.type = type;
    }

    /**
     * Retrieve all domain.entities.
     *
     * @return List<domain.entities>
     */
    public List<T> get() throws DatabaseException {
        EntityManager entityManager = JpaUtil.getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<T> cq = cb.createQuery(type);
            cq.from(type);
            return entityManager.createQuery(cq).getResultList();
        } catch (Exception e) {
            System.out.println(e.getMessage());

            throw new DatabaseException(e.getMessage());
        } finally {
            entityManager.close();
        }
    }

    /**
     * Find domain.entities.
     *
     * @param id domain.entities id
     * @return domain.entities
     */
    public T find(Long id) {
        EntityManager entityManager = JpaUtil.getEntityManager();
        try {
            return entityManager.find(type, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }
        return null;
    }

    /**
     * Create domain.entities.
     *
     * @param entity domain.entities model.
     * @return created domain.entities
     */
    public T create(T entity) {
        EntityManager entityManager = JpaUtil.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(entity);
            entityManager.getTransaction().commit();

            return entity;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }
        return null;
    }

    /**
     * Update domain.entities.
     *
     * @param entity domain.entities model
     * @return updated domain.entities
     */
    public T update(T entity) {
        EntityManager entityManager = JpaUtil.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entity = entityManager.merge(entity);
            entityManager.getTransaction().commit();

            return entity;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }
        return null;
    }

    /**
     * Remove domain.entities.
     *
     */
    public void remove(Long id) {
        EntityManager entityManager = JpaUtil.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            T entity = find(id);
            entityManager.remove(entityManager.merge(entity));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }
    }

    public List<T> get(String sql,  List<Object> parameters){
        EntityManager entityManager = JpaUtil.getEntityManager();
        Query query = entityManager.createQuery(sql);
        int i = 1;
        for(Object parameter : parameters){
            query.setParameter("arg" + i, parameter);
            i++;
        }

        List<T> results = new ArrayList<>();
        try {
            results.addAll(query.getResultList());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            entityManager.close();
        }

        return results;

    }
}
