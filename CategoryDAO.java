package infrastructure.data.dao;

import domain.entities.Category;
import infrastructure.data.interfaces.ICategoryDAO;

public class CategoryDAO extends GenericDAO<Category> implements ICategoryDAO {
    public CategoryDAO() {
        super(Category.class);
    }
}
