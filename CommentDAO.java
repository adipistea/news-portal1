package infrastructure.data.dao;

import domain.entities.Comment;
import domain.entities.Friendship;
import infrastructure.JpaUtil;
import infrastructure.data.interfaces.ICommentDAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CommentDAO extends GenericDAO<Comment> implements ICommentDAO {
    public CommentDAO() {
        super(Comment.class);
    }

    public List<Comment> getCommentsForNews(Long newsId){
        String sql = new String("select c from " + Comment.class.getName() + " c  where c.news.id =:arg1");
        ArrayList<Object> parameters = new ArrayList<>();
        parameters.add(newsId);
        List<Comment> comments = get(sql, parameters);
        return comments;
    }


    public List<Comment> getCommentsForUser(Long id){

        String sql = new String("select c from " + Comment.class.getName() + " c  where c.id =:arg1");
        ArrayList<Object> parameters = new ArrayList<>();
        parameters.add(id);
        List<Comment> comments = get(sql, parameters);
        return comments;
//        EntityManager entityManager = JpaUtil.getEntityManager();
//        Query query = entityManager.createQuery(sql);
//
//            query.setParameter("arg1", id);
//
//
//        List<Comment> results = new ArrayList<>();
//        try {
//            results.addAll(query.getResultList());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        } finally {
//            entityManager.close();
//        }
//
//        return results;
//        EntityManager entityManager = JpaUtil.getEntityManager();
//        Query query = entityManager.createNativeQuery(new String("Select * From comments Where comments.FriendId =" + id));
//
//
//        List<Comment> results = new ArrayList<>();
//        try {
//            results.addAll(query.getResultList());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        } finally {
//            entityManager.close();
//        }
//
//        return results;

    }

    public List<Comment> getCommentsForFriend(Long id) {

        String sql = new String("select c from " + Comment.class.getName() + " c  where c.friend.id =:arg1");
        ArrayList<Object> parameters = new ArrayList<>();
        parameters.add(id);
        List<Comment> comments = get(sql, parameters);
        return comments;
    }
}
