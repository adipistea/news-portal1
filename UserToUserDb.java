package common.mapping;

import javafx.model.User;

import java.util.List;


public class UserToUserDb implements GenericMapper<User, domain.entities.User> {
    @Override
    public domain.entities.User map(User source) {

        return source!=null ?
                new domain.entities.User(source.getId(), source.getFirstName(), source.getLastName(), source.getUserName(), null) : null;
    }
}
